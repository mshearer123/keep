package nz.co.mshearer.keep.contact;


import nz.co.mshearer.keep.Contact;
import nz.co.mshearer.keep.Presenter;

public class ContactPresenter implements Presenter {

    private ContactView view;

    public ContactPresenter(ContactView view) {
        this.view = view;
    }

    public void initialize(Contact contact) {
        this.view.renderName(contact.getName());
        this.view.renderPhoneNumber(contact.getPhoneNumber());
        this.view.renderProfilePhoto(contact.getProfileUrl());
        this.view.showContent();
    }

    public void handleShowAddressClick(Contact contact) {
        StringBuilder builder = new StringBuilder(contact.getAddress());
        builder.append(" " + contact.getCity());
        builder.append(" " + contact.getPostCode());
        this.view.renderAddress(builder.toString());
    }

    @Override
    public void ready() {
        this.view.showLoading();
    }

    @Override
    public void resume() {
        //TODO
    }

    @Override
    public void pause() {
        //TODO
    }

    @Override
    public void destroy() {
        //TODO
    }
}
