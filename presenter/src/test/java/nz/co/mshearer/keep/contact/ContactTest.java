package nz.co.mshearer.keep.contact;

import org.junit.Test;
import org.mockito.Mock;

import nz.co.mshearer.keep.BaseMockTest;
import nz.co.mshearer.keep.Contact;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Mockito.verify;


public class ContactTest extends BaseMockTest {

    @Mock
    ContactView mockContactView;
    private ContactPresenter contactPresenter;
    private Contact contact;


    @Override
    public void setUp() {
        super.setUp();
        this.contactPresenter = new ContactPresenter(mockContactView);

        this.contact = new Contact("Matthew Shearer",
                "0279028892",
                "https://media.licdn.com/mpr/mpr/shrinknp_400_400/p/7/000/2aa/1f6/2fcbf11.jpg");
        this.contact.setAddress("35B Grafton Road");
        this.contact.setCity("Wellington");
        this.contact.setPostCode("6011");
    }

    @Test
    public void contact_presenterReady_hideContentShowLoading() {
        this.contactPresenter.ready();
        verify(mockContactView).showLoading();
    }

    @Test
    public void contact_renderContact_contactRendered() {
        this.contactPresenter.initialize(contact);
        verify(this.mockContactView).showContent();
        verify(this.mockContactView).renderName("Matthew Shearer");
        verify(this.mockContactView).renderPhoneNumber("0279028892");
        verify(this.mockContactView).renderProfilePhoto("https://media.licdn.com/mpr/mpr/shrinknp_400_400/p/7/000/2aa/1f6/2fcbf11.jpg");
    }

    @Test
    public void contact_renderAddress_addressRendered() {

        this.contactPresenter.handleShowAddressClick(contact);
        verify(this.mockContactView).renderAddress("35B Grafton Road Wellington 6011");


    }
}
