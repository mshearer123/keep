package nz.co.mshearer.keep;

import org.junit.Before;
import org.mockito.MockitoAnnotations;

/**
 * Class used to configure any common behaviour in all tests/
 */
public class BaseMockTest {

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
}
