package nz.co.mshearer.keep;

import java.io.Serializable;

public class Contact implements Serializable {

    private String name;
    private String phoneNumber;
    private String profileUrl;
    private String address;
    private String city;
    private String postCode;


    public Contact(String name, String phoneNumber, String profileUrl) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.profileUrl = profileUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public static class Builder {

        //required fields
        private String name;
        private String phoneNumber;
        private String profileUrl;

        //optional fields
        private String address;
        private String city;
        private String postCode;

        public Builder(String name, String phoneNumber, String profileUrl) {
            this.name = name;
            this.phoneNumber = phoneNumber;
            this.profileUrl = profileUrl;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public Builder postCode(String postCode) {
            this.postCode = postCode;
            return this;
        }

        public Contact build() {
            return new Contact(this);
        }
    }

    private Contact(Builder builder) {
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.profileUrl = builder.profileUrl;
        this.address = builder.address;
        this.city = builder.city;
        this.postCode = builder.postCode;
    }

}
