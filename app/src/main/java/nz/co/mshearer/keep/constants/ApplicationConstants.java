package nz.co.mshearer.keep.constants;

public class ApplicationConstants {

    public static final String LINK_PRIVACY_POLICY = "http://keepme.co.nz/privacy-policy";
    public static final String LINK_PERSONAL_LEGAL = "http://keepme.co.nz/Legal/Personal";
}