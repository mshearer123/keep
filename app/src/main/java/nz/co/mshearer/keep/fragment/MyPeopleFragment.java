package nz.co.mshearer.keep.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import icepick.Icicle;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import jp.wasabeef.recyclerview.animators.adapters.ScaleInAnimationAdapter;
import nz.co.mshearer.keep.Contact;
import nz.co.mshearer.keep.R;
import nz.co.mshearer.keep.fragment.dialog.ContactDialogFragment;

public class MyPeopleFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = MyPeopleFragment.class.getName();

    @Bind(R.id.recyclerview_contacts)
    protected RecyclerView recyclerView;

    @Bind(R.id.swiperefresh)
    protected SwipeRefreshLayout swipeRefreshLayout;

    @Bind(R.id.tab_layout)
    protected TabLayout tabLayout;

    @Bind(R.id.fab)
    protected FloatingActionButton fab;

    @Icicle ArrayList<Contact> contacts;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mypeople, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("My People");
        if (this.contacts == null) {
            String[] contacts = getResources().getStringArray(R.array.contacts);
            new LoadContactsTask().execute(contacts);
        } else {
            setContacts(this.contacts);
        }


        setHasOptionsMenu(true);
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.recyclerView.setItemAnimator(new SlideInUpAnimator());

        this.swipeRefreshLayout.setOnRefreshListener(this);

        this.tabLayout.addTab(this.tabLayout.newTab().setText(getString(R.string.tab_all)));
        this.tabLayout.addTab(this.tabLayout.newTab().setText(getString(R.string.tab_invite)));
        this.tabLayout.addTab(this.tabLayout.newTab().setText(getString(R.string.tab_swap)));


        // add listener to show hide contacts page.  Remove when actually fragments are added.
        this.tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {


                switch (tab.getPosition()) {
                    case 0:
                        MyPeopleFragment.this.recyclerView.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        MyPeopleFragment.this.recyclerView.setVisibility(View.GONE);
                        break;
                    case 2:
                        MyPeopleFragment.this.recyclerView.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        this.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Add contact panel", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search, menu);
    }

    private void setContacts(ArrayList<Contact> contacts) {
        ContactsAdapter contactsAdapter = new ContactsAdapter(contacts);
        this.recyclerView.setAdapter(new ScaleInAnimationAdapter(contactsAdapter));
    }

    @Override
    public void onRefresh() {
        this.recyclerView.setVisibility(View.GONE);

        // fake a response delay so refresh wheel is displayed
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MyPeopleFragment.this.recyclerView.setVisibility(View.VISIBLE);
                MyPeopleFragment.this.swipeRefreshLayout.setRefreshing(false);
                MyPeopleFragment.this.recyclerView.getAdapter().notifyDataSetChanged();
            }
        }, 2000);
    }


    private class LoadContactsTask extends AsyncTask<String[], Void, ArrayList<Contact>> {

        @Override
        protected ArrayList<Contact> doInBackground(String[]... params) {
            ArrayList<Contact> contactsList = new ArrayList<>();
            String[] contacts = params[0];

            //add unnecessary loop to generate more contacts.
            for (int i = 0; i < 20; i++) {
                for (String contactString : contacts) {
                    String[] tokens = contactString.split(",");


                    String name = tokens[0].trim();
                    String phoneNumber = tokens[1].trim();
                    String photoUrl = tokens[2].trim();

                    Contact.Builder contactBuilder = new Contact.Builder(name, phoneNumber, photoUrl);
                    contactBuilder.address("35B grafton road");
                    contactBuilder.city("Wellington");
                    contactBuilder.postCode("6011");
                    contactsList.add(contactBuilder.build());
                }
            }

            return contactsList;
        }


        @Override
        protected void onPostExecute(ArrayList<Contact> contacts) {
            super.onPostExecute(contacts);
            setContacts(contacts);
        }
    }


    public class ContactsAdapter extends RecyclerView.Adapter<ContactViewHolder> {

        private ArrayList<Contact> contacts;

        public ContactsAdapter(ArrayList<Contact> contacts) {
            this.contacts = contacts;
        }

        @Override
        public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_contact_item, parent, false);
            return new ContactViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ContactViewHolder holder, int position) {
            final Contact contact = contacts.get(position);

            holder.textViewName.setText(contact.getName());
            holder.textViewPhone.setText(contact.getPhoneNumber());
            Transformation transformation = new RoundedTransformationBuilder()
                    .cornerRadiusDp(30)
                    .borderColor(R.color.colorPrimaryDark)
                    .borderWidthDp(1)
                    .oval(true)
                    .build();

            Picasso.with(getActivity()).load(contact.getProfileUrl()).transform(transformation).noFade().into(holder.imageViewIcon);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContactDialogFragment.startDialog(contact, getChildFragmentManager());
                }
            });
        }

        @Override
        public int getItemCount() {
            return contacts.size();
        }
    }


    public class ContactViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.textview_name)
        protected TextView textViewName;

        @Bind(R.id.textview_phone)
        protected TextView textViewPhone;

        @Bind(R.id.imageview_icon)
        protected ImageView imageViewIcon;


        public ContactViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}