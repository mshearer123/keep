package nz.co.mshearer.keep.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import nz.co.mshearer.keep.R;
import nz.co.mshearer.keep.activity.NavigationActivity;
import nz.co.mshearer.keep.util.SharedPreferenceUtil;

public class AccountFragment extends BaseFragment {

    public static final String TAG = AccountFragment.class.getName();


    @Bind(R.id.edit_text_name)
    protected EditText editTextName;

    @Bind(R.id.edit_text_phone)
    protected EditText editTextPhone;

    @Bind(R.id.button_save)
    protected Button buttonSave;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Account");


        this.buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString();
                String phone = editTextPhone.getText().toString();

                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(getActivity(), "Name missing", Toast.LENGTH_LONG).show();
                    return;
                }

                if (TextUtils.isEmpty(phone)) {
                    Toast.makeText(getActivity(), "Phone missing", Toast.LENGTH_LONG).show();
                    return;
                }

                SharedPreferenceUtil.saveName(getActivity(), name);
                Snackbar.make(buttonSave, "Saved", Snackbar.LENGTH_LONG).show();

                // normally this should be avoided.  But this app only has one activity so far.
                ((NavigationActivity) getActivity()).setUserName(name);
            }
        });
    }
}
