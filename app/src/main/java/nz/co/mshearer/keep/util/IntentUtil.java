package nz.co.mshearer.keep.util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.List;

import nz.co.mshearer.keep.R;

public class IntentUtil {

    private static final String URL_CHROME_PLAY_STORE = "https://play.google.com/store/apps/details?id=com.android.chrome";

    public static void startBrowserIntent(Context context, String url) {
        //no browser app was available, start intent to get browser app
        Intent browseIntent = getBrowseUrlIntent(url);
        if (isIntentAvailable(context, browseIntent)) {
            context.startActivity(browseIntent);
        } else {
            //if there isn't, take user to the play store.
            Intent chromePlayStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL_CHROME_PLAY_STORE));
            //start play store intent
            if (isIntentAvailable(context, chromePlayStoreIntent)) {
                context.startActivity(chromePlayStoreIntent);
            } else {
                Toast.makeText(context, context.getString(R.string.error_no_browser), Toast.LENGTH_LONG).show();
            }
        }
    }

    public static boolean startEmailIntent(Context context, String emailAddress, String subject, String body) {
        try {
            context.startActivity(IntentUtil.getEmailIntent(emailAddress, subject, body));
        } catch (ActivityNotFoundException exception) {
            // this will fail if there is no email activity, such as on a emulator.
            return false;
        }

        return true;

    }

    public static void startPhoneIntent(Context context, String phoneNumber) {
        context.startActivity(IntentUtil.getPhoneIntent(phoneNumber));
    }

    public static Intent getBrowseUrlIntent(String url) {
        if (TextUtils.isEmpty(url)) {
            throw new IllegalArgumentException("the url was empty");
        }
        return new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
    }

    public static boolean isIntentAvailable(Context context, Intent intent) {
        final PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    public static Intent getPhoneIntent(String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber)) {
            throw new IllegalArgumentException("phone number was empty");
        }
        return new Intent(Intent.ACTION_DIAL, Uri.parse(String.format("tel:%s", phoneNumber)));
    }

    public static Intent getEmailIntent(String emailAddress, String subject) {
        return getEmailIntent(emailAddress, subject, null);
    }

    public static Intent getEmailIntent(String emailAddress, String subject, String body) {
        if (TextUtils.isEmpty(emailAddress)) {
            throw new IllegalArgumentException("email address was empty");
        }

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");

        intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{emailAddress});

        if (null != subject) {
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        }
        if (null != body) {
            intent.putExtra(android.content.Intent.EXTRA_TEXT, body);
        }

        return intent;
    }
}
