package nz.co.mshearer.keep.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.Icepick;
import icepick.Icicle;
import nz.co.mshearer.keep.Contact;
import nz.co.mshearer.keep.R;
import nz.co.mshearer.keep.contact.ContactPresenter;
import nz.co.mshearer.keep.contact.ContactView;

public class ContactDialogFragment extends DialogFragment implements ContactView {

    public static final String TAG = ContactDialogFragment.class.getName();

    private static final String EXTRA_CONTACT = "extra.contact";


    @Bind(R.id.content)
    protected View content;

    @Bind(R.id.progress)
    protected ProgressBar progressBar;

    @Bind(R.id.text_view_name)
    protected TextView textViewName;

    @Bind(R.id.text_view_phone)
    protected TextView textViewPhone;

    @Bind(R.id.text_view_address)
    protected TextView textViewAddress;

    @Bind(R.id.image_view_photo)
    protected ImageView imageViewPhoto;

    @Bind(R.id.button_show_address)
    protected Button buttonShowAddress;


    private ContactPresenter presenter;

    @Icicle
    Contact contact;

    /**
     * convenient start method to launch the dialog with a contact.
     */
    public static void startDialog(Contact contact, FragmentManager fragmentManager) {
        newIntstance(contact).show(fragmentManager, TAG);
    }

    public static ContactDialogFragment newIntstance(Contact contact) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CONTACT, contact);
        ContactDialogFragment fragment = new ContactDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_contact, container);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
        this.presenter = new ContactPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.contact = (Contact) getArguments().get(EXTRA_CONTACT);
        this.presenter.initialize(this.contact);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public void renderProfilePhoto(String url) {
        Glide.with(this).load(url).into(this.imageViewPhoto);
    }

    @Override
    public void renderName(String name) {
        this.textViewName.setText(name);
    }

    @Override
    public void renderPhoneNumber(String phoneNumber) {
        this.textViewPhone.setText(phoneNumber);
    }

    @Override
    public void renderAddress(String address) {
        this.textViewAddress.setVisibility(View.VISIBLE);
        this.buttonShowAddress.setVisibility(View.GONE);
        this.textViewAddress.setText(address);
    }

    @Override
    public void showLoading() {
        this.progressBar.setVisibility(View.VISIBLE);
        this.content.setVisibility(View.GONE);
    }

    @Override
    public void showContent() {
        this.progressBar.setVisibility(View.GONE);
        this.content.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.button_show_address)
    public void onButtonClicked(View view) {
        this.presenter.handleShowAddressClick(this.contact);
    }
}
