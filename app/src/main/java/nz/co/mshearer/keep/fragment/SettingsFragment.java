package nz.co.mshearer.keep.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import nz.co.mshearer.keep.R;

public class SettingsFragment extends BaseFragment {

    public static final String TAG = SettingsFragment.class.getName();

    @Bind(R.id.layout_show_non_nz_numbers)
    protected View layoutShowNonNz;

    @Bind(R.id.layout_forget_me)
    protected View layoutForgetMe;

    @Bind(R.id.layout_keep_me_sync)
    protected View layoutKeepSync;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Settings");
        configureShowNonNzView();
        configureForgetMeView();
        configureKeepMeSyncView();
    }

    private void configureShowNonNzView() {
        SettingsViewHolder viewHolder = new SettingsViewHolder();
        ButterKnife.bind(viewHolder, this.layoutShowNonNz);
        viewHolder.textview.setText("Show non-Nz Mobile Numbers");
    }

    private void configureForgetMeView() {
        SettingsViewHolder viewHolder = new SettingsViewHolder();
        ButterKnife.bind(viewHolder, this.layoutForgetMe);
        viewHolder.textview.setText("ForgetMe");
    }

    private void configureKeepMeSyncView() {
        SettingsViewHolder viewHolder = new SettingsViewHolder();
        ButterKnife.bind(viewHolder, this.layoutKeepSync);
        viewHolder.textview.setText("KeepMe Sync");
    }

    public class SettingsViewHolder {
        @Bind(R.id.textview)
        protected TextView textview;
    }
}
