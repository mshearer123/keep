package nz.co.mshearer.keep.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import nz.co.mshearer.keep.R;
import nz.co.mshearer.keep.constants.ApplicationConstants;
import nz.co.mshearer.keep.fragment.AccountFragment;
import nz.co.mshearer.keep.fragment.MyPeopleFragment;
import nz.co.mshearer.keep.fragment.OrganisationsFragment;
import nz.co.mshearer.keep.fragment.SettingsFragment;
import nz.co.mshearer.keep.util.IntentUtil;
import nz.co.mshearer.keep.util.SharedPreferenceUtil;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Bind(R.id.toolbar)
    protected Toolbar toolbar;
    @Bind(R.id.drawer_layout)
    protected DrawerLayout drawer;
    @Bind(R.id.nav_view)
    protected NavigationView navigationView;
    @Bind(R.id.nav_view_footer)
    protected NavigationView footerNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        ButterKnife.bind(this);

        configureToolbar(this.toolbar);
        configureNavigationDrawer(this.navigationView, this.footerNavigationView);
    }

    private void configureToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, this.drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        this.drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    private void configureNavigationDrawer(NavigationView navigationView, NavigationView footerNavigationView) {
        navigationView.setNavigationItemSelectedListener(this);
        footerNavigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(1).setChecked(true);

        Bundle args = new Bundle();
        changePage(MyPeopleFragment.TAG, args);

        String name = SharedPreferenceUtil.retrieveName(this);
        setUserName(name);

    }

    public void setUserName(String userName) {
        if (!TextUtils.isEmpty(userName)) {
            TextView textView = (TextView) this.navigationView.getHeaderView(0).findViewById(R.id.text_view_username);
            textView.setText(userName);
            textView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if (this.drawer.isDrawerOpen(GravityCompat.START)) {
            this.drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        Bundle args = new Bundle();

        int id = item.getItemId();

        if (id == R.id.nav_current_details) {
            changePage(AccountFragment.TAG, args);
        } else if (id == R.id.nav_people) {
            changePage(MyPeopleFragment.TAG, args);
        } else if (id == R.id.nav_organisations) {
            changePage(OrganisationsFragment.TAG, args);
        } else if (id == R.id.nav_settings) {
            changePage(SettingsFragment.TAG, args);
        } else if (id == R.id.nav_privacy_policy) {
            IntentUtil.startBrowserIntent(this, ApplicationConstants.LINK_PRIVACY_POLICY);
        } else if (id == R.id.nav_legal_terms) {
            IntentUtil.startBrowserIntent(this, ApplicationConstants.LINK_PERSONAL_LEGAL);
        } else if (id == R.id.nav_feeback) {
            if (!IntentUtil.startEmailIntent(this, "help@keep.com", "feedback", "")) {
                Toast.makeText(this, "Email failed", Toast.LENGTH_LONG).show();
            }
        }

        this.drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void changePage(String fragmentName, Bundle args) {
        Fragment fragment = Fragment.instantiate(this, fragmentName);
        fragment.setArguments(args);
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.layout_fragment_container, fragment).commit();
    }
}
