package nz.co.mshearer.keep.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceUtil {

    private static SharedPreferences getSharedPreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                "nz.co.mshearer.keep", Context.MODE_PRIVATE);
        return sharedPreferences;

    }

    public static void saveName(Context context, String name) {
        getSharedPreferences(context).edit().putString("NAME", name).apply();
    }

    public static String retrieveName(Context context) {
        return getSharedPreferences(context).getString("NAME", null);
    }

}
