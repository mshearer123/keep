package nz.co.mshearer.keep.contact;

import nz.co.mshearer.keep.LoadDataView;

public interface ContactView extends LoadDataView {

    void renderProfilePhoto(String url);

    void renderName(String name);

    void renderPhoneNumber(String phoneNumber);

    void renderAddress(String address);

}
